package dev.gly.testPlugin;

import dev.gly.planegeometry.Shape;
import dev.gly.testPlugin.commands.SetPolygon;
import dev.gly.testPlugin.events.PlayerEntryArea;
import org.bukkit.plugin.java.JavaPlugin;

public class TestPlugin extends JavaPlugin {
    private Shape shape = null;
    SetPolygon setPolygon = new SetPolygon(this);

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Shape getShape() {
        return shape;
    }

    @Override
    public void onLoad() {
        getLogger().info("plug loaded");
    }

    @Override
    public void onDisable() {
        getLogger().info("plug disabled");
    }

    @Override
    public void onEnable() {
        getLogger().info("plug enabled");
        getServer().getPluginManager().registerEvents(new PlayerEntryArea(this), this);
        getServer().getPluginManager().registerEvents(setPolygon, this);
        getCommand("start").setExecutor(setPolygon);
        getCommand("stop").setExecutor(setPolygon);

    }

}
