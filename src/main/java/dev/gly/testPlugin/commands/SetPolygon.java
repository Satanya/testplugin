package dev.gly.testPlugin.commands;

import dev.gly.planegeometry.Point;
import dev.gly.planegeometry.Polygon;
import dev.gly.testPlugin.TestPlugin;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.LinkedList;

public class SetPolygon implements CommandExecutor, Listener {
    private TestPlugin plugin;

    private boolean isSettingMode = false;
    private LinkedList<Point> pointList = new LinkedList<>();

    public SetPolygon(Plugin plugin){
        this.plugin = (TestPlugin) plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player){
            Player player = (Player) commandSender;

            switch (command.getName().toLowerCase()){
                case "start": {
                    isSettingMode = true;
                    pointList.clear(); //釋放所有點
                    player.sendMessage("你可以開始設置點位了");
                    return true;
                }

                case "stop": {
                    isSettingMode = false;
                    plugin.setShape(new Polygon(pointList));
                    player.sendMessage(ChatColor.GREEN+"已經設置完了");
                    new BukkitRunnable(){
                        @Override
                        public void run(){
                            for (Point p: plugin.getShape().getDiscretePoints(0.5)
                                 ) {
                                player.getWorld().spawnParticle(Particle.REDSTONE, new Location(player.getWorld(), p.getX(), player.getLocation().getY(), p.getY()), 3, new Particle.DustOptions(Color.YELLOW, 1));
                            }
                        }
                    }.runTaskTimer(plugin, 5, 10);
                    return true;
                }
            }
        }
        return false;
    }

    @EventHandler
    public void onPlayerClickBlock(PlayerInteractEvent e){

        if (isSettingMode && e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            pointList.add(new Point(e.getClickedBlock().getX(), e.getClickedBlock().getZ()));
            e.getPlayer().sendMessage(ChatColor.GOLD+"你設置了點");
        }
    }
}
