package dev.gly.testPlugin.events;

import dev.gly.planegeometry.Point;
import dev.gly.testPlugin.TestPlugin;
import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

public class PlayerEntryArea implements Listener {
    private TestPlugin plugin;

    public PlayerEntryArea(Plugin plugin){
        this.plugin = (TestPlugin) plugin;
    }

    @EventHandler
    public void OnPlayerEntryArea(PlayerMoveEvent e){
        if (plugin.getShape() != null && !plugin.getShape().isIncludePoint(new Point(e.getFrom().getX(), e.getFrom().getZ())) && plugin.getShape().isIncludePoint(new Point(e.getTo().getX(), e.getTo().getZ()))){
            e.getPlayer().sendTitle("§4 禁止入內","§4 你闖入了禁地！", 0, 70, 0);
            for (Point p: plugin.getShape().getDiscretePoints(1)
            ) {
                e.getPlayer().getWorld().spawnParticle(Particle.REDSTONE, new Location(e.getPlayer().getWorld(), p.getX(), e.getPlayer().getLocation().getY(), p.getY()), 3, new Particle.DustOptions(Color.RED, 2));
            }
            e.setCancelled(true);
        }
    }

}
